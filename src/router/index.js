import Vue from 'vue'
import Router from 'vue-router'

// in development-env not use lazy-loading, because lazy-loading too many pages will cause webpack hot update too slow. so only in production use lazy-loading;
// detail: https://panjiachen.github.io/vue-element-admin-site/#/lazy-loading

Vue.use(Router)

/* Layout */
import Layout from '../views/layout/Layout'

/**
* hidden: true                   if `hidden:true` will not show in the sidebar(default is false)
* alwaysShow: true               if set true, will always show the root menu, whatever its child routes length
*                                if not set alwaysShow, only more than one route under the children
*                                it will becomes nested mode, otherwise not show the root menu
* redirect: noredirect           if `redirect:noredirect` will no redirect in the breadcrumb
* name:'router-name'             the name is used by <keep-alive> (must set!!!)
* meta : {
    title: 'title'               the name show in submenu and breadcrumb (recommend set)
    icon: 'svg-name'             the icon show in the sidebar
    breadcrumb: false            if false, the item will hidden in breadcrumb(default is true)
  }
**/
export const constantRouterMap = [
  { path: '/login', component: () => import('@/views/login/index'), hidden: true },
  { path: '/404', component: () => import('@/views/404'), hidden: true },

  {
    path: '/',
    component: Layout,
    redirect: '/dashboard',
    children: [{
      path: '/dashboard',
      component: () => import('@/views/dashboard/index'),
      name: 'dashboard',
      meta: { title: 'Dashboard', icon: 'dashboard', noCache: true }
    }]
  },

  {
    path: '/users',
    component: Layout,
    redirect: '/usuarios',
    children: [{
      path: '/usuarios',
      component: () => import('@/views/users/index'),
      name: 'dashboard',
      meta: { title: 'Usuários', icon: 'user', noCache: true }
    }]
  },

  {
    component: Layout,
    path: '/users/criar',
    redirect: '/users',
    hidden: true,
    children: [{
      path: '/usuarios/criar',
      component: () => import('@/views/users/register')
    }, {
      path: '/usuarios/:id',
      component: () => import('@/views/users/register')
    }]
  },

  {
    path: '/tarefas',
    component: Layout,
    redirect: '/tarefas',
    children: [{
      path: '/tarefas',
      component: () => import('@/views/tasks/index'),
      name: 'tarefas',
      meta: { title: 'Tarefas', icon: 'list', noCache: true }
    }]
  },

  { path: '*', redirect: '/404', hidden: true }
]

export default new Router({
  // mode: 'history', //后端支持可开
  scrollBehavior: () => ({ y: 0 }),
  routes: constantRouterMap
})

export const asyncRouterMap = [

  { path: '*', redirect: '/404', hidden: true }
]
