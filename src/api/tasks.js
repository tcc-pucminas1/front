import request from '@/utils/request'

export function getTasks(pagination, filters) {
  let params = {}
  if (pagination) {
    params = {
      page_size: pagination.tamanhoPagina,
      page: pagination.paginaAtual
    }
  }

  params = {
    page_size: 50,
    page: 1
  }

  return request({
    url: '/tasks',
    method: 'get',
    params: { ...params, filters }
  })
}

export function getTaskDetail(taskId) {
  return request({
    url: `/tasks/${taskId}`,
    method: 'get'
  })
}

export function updateTask(taskId, params) {
  return request({
    url: `/tasks/${taskId}`,
    method: 'put',
    data: params
  })
}

export function insertTask(params) {
  return request({
    url: `/tasks`,
    method: 'post',
    data: params
  })
}

export function deleteTask(taskId) {
  return request({
    url: `/tasks/${taskId}`,
    method: 'delete'
  })
}

export function getDashboardTask(params) {
  return request({
    url: `/tasks/dashboard`,
    method: 'get'
  })
}
