import request from '@/utils/request'

export function listarUsuarios(paginacao) {
  let params = {}
  if (paginacao) {
    params = {
      page_size: paginacao.tamanhoPagina,
      page: paginacao.paginaAtual
    }
  }

  params = {
    page_size: 50,
    page: 1
  }

  return request({
    url: '/users',
    method: 'get',
    params: params
  })
}

export function criarUsuario(dadosFormulario) {
  return request({
    url: '/users',
    method: 'post',
    data: dadosFormulario
  })
}

export function editarUsuario(id, dadosFormulario) {
  return request({
    url: '/users/' + id,
    method: 'put',
    data: dadosFormulario
  })
}

export function exibirUsuario(id) {
  return request({
    url: '/users/' + id,
    method: 'get'
  })
}

export function removerUsuario(id) {
  return request({
    url: '/users/' + id,
    method: 'delete'
  })
}
