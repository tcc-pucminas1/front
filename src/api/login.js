import request from '@/utils/request'

export function login(username, password) {
  return request({
    url: '/auth',
    method: 'post',
    data: {
      email: username,
      password: password
    }
  })
}

export function getUser(token) {
  return request({
    url: '/user/info',
    method: 'get'
  })
}

export function logout() {
  return Promise.resolve()
}
