import axios from 'axios'
import store from '../store'
import { getToken } from '@/utils/auth'

/**
 * Instacia do axios
 * @type {AxiosInstance}
 */
const service = axios.create({
  baseURL: process.env.BASE_API, // api url
  timeout: 30000
})

/**
 * Request config
 */
service.interceptors.request.use(
  config => {
    if (store.getters.token) {
      config.headers['Authorization'] = 'bearer ' + getToken() // pega token do cabeçalho
    }
    return config
  },
  error => {
    Promise.reject(error)
  }
)

/**
 * Interceptors para o response
 */
service.interceptors.response.use(
  response => {
    return response
  },
  error => {
    return Promise.reject(error)
  }
)

export default service
