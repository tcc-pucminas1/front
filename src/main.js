import Vue from 'vue'

import Cookies from 'js-cookie'

import 'normalize.css/normalize.css' // A modern alternative to CSS resets

import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import locale from 'element-ui/lib/locale/lang/pt-br' // lang i18n

import '@/styles/index.scss' // global css

import App from './App'
import router from './router'
import store from './store'

import '@/icons' // icon'
import '@/permission' // permission control
import i18n from './lang' // Internationalization

Vue.use(ElementUI, { locale })

Vue.use(ElementUI, {
  size: Cookies.get('size') || 'medium', // set element-ui default size
  i18n: (key, value) => i18n.t(key, value)
})
Vue.config.productionTip = false

import VueMask from 'v-mask'
Vue.use(VueMask)

import VInputmask from 'v-inputmask'
Vue.use(VInputmask)

new Vue({
  el: '#app',
  router,
  store,
  i18n,
  render: h => h(App)
})
