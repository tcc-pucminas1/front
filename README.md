# iTask Frontend

> Frontend do iTask, desenvolvido em vueJs com um visual minimalista.

## Sistema
> O iTask é um gerenciador de tarefas no formato de Kanban, para fazer com que seja mais simples o gerenciamento de projetos.

## Setup

```bash
# Instalar dependencias
npm install

# Iniciar server com hot reload
npm run dev

# Build para produção
npm run build
```
